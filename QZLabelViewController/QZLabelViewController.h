//
//  ViewController.h
//  QZLabelViewController
//
//  Created by 吴清正 on 2017/4/13.
//  Copyright © 2017年 dazheng_wu. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface QZLabelItem: NSObject
@property (nonatomic, assign)CGFloat spacing;
@property (nonatomic, assign)CGFloat buttonHeight;
@property (nonatomic, strong)UIColor*titleSelectColor;
@property (nonatomic, strong)UIColor *titleNormalColor;
@end

@interface QZLabelViewController : UIViewController
@property (nonatomic, strong)NSArray *controllersArr;   ///可滑动视图控制器数组
@property (nonatomic, strong)NSArray *titleArr;         ///标签栏按钮名称数组
@property (nonatomic, assign)NSInteger currentPage;     ///设置默认选中的`UIViewController`
@property (nonatomic, strong)QZLabelItem *labelItem;
@property (nonatomic, strong)UIScrollView *tagScrollView;
@property (nonatomic, strong)UIPageViewController *pageViewController;

- (void)reloadView;
- (UIViewController *)returnTheCurrentDisplayVC;
+ (instancetype)instanceWithControllerArray:(NSArray *)__controllerArr titleArray:(NSArray *)__titleArr;
@end

