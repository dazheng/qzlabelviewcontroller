//
//  AppDelegate.h
//  QZLabelViewController
//
//  Created by 吴清正 on 2017/4/13.
//  Copyright © 2017年 dazheng_wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

