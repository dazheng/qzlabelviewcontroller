//
//  AppDelegate.m
//  QZLabelViewController
//
//  Created by 吴清正 on 2017/4/13.
//  Copyright © 2017年 dazheng_wu. All rights reserved.
//

#import "AppDelegate.h"
#import "QZLabelViewController.h"
#import "QZPageViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    QZLabelViewController *vc = [QZLabelViewController instanceWithControllerArray:@[[QZPageViewController new],
                                                                                     [QZPageViewController new],
                                                                                     [QZPageViewController new],
                                                                                     [QZPageViewController new],
                                                                                     [QZPageViewController new],
                                                                                     [QZPageViewController new],
                                                                                     [QZPageViewController new]]
                                                                        titleArray:@[@"全部",
                                                                                     @"待付款",
                                                                                     @"待接单",
                                                                                     @"进行中",
                                                                                     @"待确认",
                                                                                     @"已完成",
                                                                                     @"已取消"]];

    QZLabelItem *labelItem = [[QZLabelItem alloc] init];
    labelItem.titleSelectColor = [UIColor brownColor];
    labelItem.titleNormalColor = [UIColor blackColor];
    labelItem.buttonHeight = 55.0;
    vc.labelItem = labelItem;
    vc.tagScrollView.backgroundColor = [UIColor lightGrayColor];
    vc.currentPage = 2;

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
